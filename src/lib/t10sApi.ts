import axios from 'axios';

export class Tus10SegundosApi {
  async getProduct(options: { slug: string }) {
    try {
      const response = await axios({
        url: 'https://graph.tus10segundos.com/graphql',
        method: 'post',
        data: {
          query: `
          {
            product: getProduct(slug: "${options.slug}"){
              slug
            }
          }
          `,
        },
      });
      const product = response.data.data.product as { slug: string };
      return product;
    } catch (error) {
      console.log(error.response.data.errros);
      return null;
    }
  }

  async getOrder(options: { orderNumber: string }) {
    try {
      const response = await axios({
        url: 'https://graph.tus10segundos.com/graphql',
        method: 'post',
        data: {
          query: `
          {
            order: getOrder(orderNumber: ${options.orderNumber}){
              clientLink
            }
          }
          `,
        },
      });
      const order = response.data.data.order as { clientLink: string };
      return order;
    } catch (error) {
      console.log(error.response.data.errros);
      return null;
    }
  }
}
