export interface Order {
  orderNumber: number;
  clientLink: string;
}

export interface Product {
  slug: string;
}
